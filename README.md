# node.js-clean-architecture.
[A use case of Clean Architecture in Node.js comprising of Express.js, MongoDB, Redis and Docker as the main (but replaceable) infrastructure.](https://github.com/panagiop/node.js-clean-architecture)
https://www.bezkoder.com/jwt-refresh-token-node-js/

# swagger
[At the moment nothing should have changed inside our responses (controller). 
But under the hood our controller layer talks now with our service layer.
Inside our service methods we'll be handling our business logic like transforming data structures 
and communicating with our Database Layer.]
(https://www.freecodecamp.org/news/rest-api-design-best-practices-build-a-rest-api)
https://github.com/howardmann/clean-node

# MongoDB
[https://prashix.medium.com/setup-mongodb-replicaset-with-authentication-enabled-using-docker-compose-5edd2ad46a90]
mongo didalm docker, lihat di Documents/docker/mongodb
untuk menjalankan ikuti perintah ini
- tambahkan hosts untuk nama replica set nya, lihat file private/etc/host dan samakan dengan nama replica set yg akan di running
- masuk ke dlm folder Documents/docker/mongodb, jln kan :
  - **docker-compose -f mongo-compose.yaml up -d**
  - **docker exec mongo-0 scripts/setup.sh**
- mongo-0 nama replica set yg juga di set didlm file hosts

# NOTE
routes/user => setiap buat administrator baru middlewarenya dilepas
untuk build-server tinggal di ugly atau di encrypt

# PROSES BISNIS
Inventory (vessels) => daftar vessels yang akan di service, untuk ui/ux dimapping lg apakah digabung ke wo atau dibuat jadi 2 menu vessels dan inventory
Parts Inventory => daftar part yang ada disetiap kapal, dimapping lg proses bisnisnya, karena ada part yg kapal beli sendiri dan ada yang dari pusat, dan jika digabung harus ada flagnya brp yg dari pusat dan brp yg dari kapal
Vendors => daftar vendor yang akan mengerjakan workorder / perbaikan, dan untuk partnya bisa dari vendor sndri atau dari stock kapal, list vendor yg create, update dan delete hanya pusat kapal hanya bisa milih / lihat
Workorder => datanya gabungan dari [inventory, part, vendor(jika dipilih)]
TSAR => header->detail->sub detail, yang dicelist hanya detail sisanya keterangan/list saja
CREW => untuk skrg data crew kapal disatukan dengan table user, kedepannya table user dan crew dipisah
  -> auth/login => ubah get datanya jika sdh dipisah
  -> mongoDB/collections/user.js
AUTH => refreshToken di FrontEnd blm dimplement, dikarenakan harus mapping mana2 API yg tidak butuh refresh token, jika user dan atau trx sdh banyak (misal: 1000 req/sec) baru diimplement

# RULES
error/warning Messages => harus selalu dibuat array

# DAFTAR PUSTAKA
https://www.topcoder.com/thrive/articles/geospatial-location-queries-in-mongodb