import Joi from 'joi';

import Admins from '../models/adminModel';

export default function adminValidator() {
  const throwErr = new Error('ERROR Admin Validation !');

  const updateAdmin = async ({ _id, fullname, email, password, roles }) => {
    const schemaAdmins = Joi.object({
      fullname: Joi.string().max(45).required(),
      email: Joi.string().max(45).required(),
      password: Joi.string().allow(null, '').max(45),
      roles: Joi.string().max(35).required(),
      _id: Joi.string().max(35).required()
    });

    const { error } = schemaAdmins.validate(
      { _id, fullname, email, password, roles },
      { abortEarly: false }
    );
    if (error) {
      throwErr.statusCode = 400;
      throwErr.customMessage = error.details.map((el) =>
        el.message.replace(/"/g, '')
      );
      throw throwErr;
    }

    // validation email
    const admins = await Admins().findAdmin({ email, notId: _id });
    if (admins) {
      throwErr.statusCode = 400;
      throwErr.customMessage = ['ADMIN sudah didaftarkan !'];
      throw throwErr;
    }

    return true;
  };

  const addNewAdmin = async ({ fullname, email, password, roles }) => {
    const schemaAdmins = Joi.object({
      fullname: Joi.string().max(45).required(),
      email: Joi.string().max(45).required(),
      password: Joi.string().max(30).required(),
      roles: Joi.string().max(35).required()
    });

    const { error } = schemaAdmins.validate(
      { fullname, email, password, roles },
      { abortEarly: false }
    );
    if (error) {
      throwErr.statusCode = 400;
      throwErr.customMessage = error.details.map((el) =>
        el.message.replace(/"/g, '')
      );
      throw throwErr;
    }

    // validation email
    const admins = await Admins().findAdmin({ email });
    if (admins) {
      throwErr.statusCode = 400;
      throwErr.customMessage = ['ADMIN sudah didaftarkan !'];
      throw throwErr;
    }

    return true;
  };

  return {
    updateAdmin,
    addNewAdmin
  };
}
