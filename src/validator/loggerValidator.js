import Joi from 'joi';
import companyModel from '../models/companyModel';
import Schema from '../database/mongoDB/collections';

const { Loggers } = Schema;

export default function loggerValidator() {
  const throwErr = new Error('Auth Validation');

  const addNewLogger = async (params) => {
    const { uid, uidAlias, address, coordinates, companies, status, sensors } =
      params;

    const schemaRoles = Joi.object({
      uid: Joi.string().allow(null, '').max(45),
      uidAlias: Joi.string().required().max(45),
      address: Joi.string().required().max(200),
      companies: Joi.string().required().max(30),
      status: Joi.string().required().max(10),
      coordinates: Joi.array().items(Joi.number().max(999).required())
    });

    const { error } = schemaRoles.validate(
      { uid, uidAlias, address, companies, status, coordinates },
      { abortEarly: false }
    );
    if (error) {
      throwErr.statusCode = 400;
      throwErr.customMessage = error.details.map((el) =>
        el.message.replace(/"/g, '')
      );
      throw throwErr;
    }

    // validate sensors
    const schemaSensors = Joi.array().items(
      Joi.object().keys({
        name: Joi.string().max(20).required(),
        merk: Joi.string().max(20).required(),
        lowerLimit: Joi.number().max(99999).required(),
        upperLimit: Joi.number().max(99999).required(),
        serialNumber: Joi.string().max(45).allow(null, '')
      })
    );
    const sensorsValidate = schemaSensors.validate(sensors, {
      abortEarly: false
    });
    if (sensorsValidate.error) {
      throwErr.statusCode = 400;
      throwErr.customMessage = sensorsValidate.error.details.map((el) =>
        el.message.replace(/"/g, '')
      );
      throw throwErr;
    }

    // validate company
    const company = await companyModel().findCompany({ _id: companies });
    if (!company) {
      throwErr.statusCode = 400;
      throwErr.customMessage = 'COMPANY INVALID !';
      throw throwErr;
    }

    // validate uid
    const logger = await Loggers.findOne({ uidAlias });
    if (logger && logger?.uid === uid.toUpperCase()) {
      throwErr.statusCode = 400;
      throwErr.customMessage = 'UID / UID ALIAS sudah terdaftar !';
      throw throwErr;
    }

    return true;
  };

  const updateLogger = async (params) => {
    const {
      _id,
      uid,
      uidAlias,
      address,
      coordinates,
      companies,
      status,
      sensors
    } = params;

    const schemaRoles = Joi.object({
      _id: Joi.string().required().max(35),
      uid: Joi.string().allow(null, '').max(45),
      uidAlias: Joi.string().required().max(45),
      address: Joi.string().required().max(200),
      companies: Joi.string().required().max(30),
      status: Joi.string().required().max(10),
      coordinates: Joi.array().required().items(Joi.number())
    });

    const { error } = schemaRoles.validate(
      { _id, uid, uidAlias, address, companies, status, coordinates },
      { abortEarly: false }
    );
    if (error) {
      throwErr.statusCode = 400;
      throwErr.customMessage = error.details.map((el) =>
        el.message.replace(/"/g, '')
      );
      throw throwErr;
    }

    // validate sensors
    const schemaSensors = Joi.array().items(
      Joi.object().keys({
        name: Joi.string().max(20).required(),
        merk: Joi.string().max(20).required(),
        lowerLimit: Joi.number().required(),
        upperLimit: Joi.number().required(),
        serialNumber: Joi.string().max(45).allow(null, '')
      })
    );
    const sensorsValidate = schemaSensors.validate(sensors, {
      abortEarly: false
    });
    if (sensorsValidate.error) {
      throwErr.statusCode = 400;
      throwErr.customMessage = sensorsValidate.error.details.map((el) =>
        el.message.replace(/"/g, '')
      );
      throw throwErr;
    }

    // validate uid
    const logger = await Loggers.findOne({
      _id: { $ne: _id },
      uidAlias
    });
    const isValidUid =
      logger?.uid === uid.toUpperCase() ||
      logger?.uidAlias === uidAlias.toUpperCase();
    if (isValidUid) {
      throwErr.statusCode = 400;
      throwErr.customMessage = 'UID / UID ALIAS sudah terdaftar !';
      throw throwErr;
    }

    // validate company
    const company = await companyModel().findCompany({ _id: companies });
    if (!company) {
      throwErr.statusCode = 400;
      throwErr.customMessage = 'COMPANY INVALID !';
      throw throwErr;
    }

    return true;
  };

  return {
    addNewLogger,
    updateLogger
  };
}
