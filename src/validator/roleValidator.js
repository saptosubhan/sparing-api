import Joi from 'joi';
import _ from 'lodash';

import Roles from '../models/roleModel';
import config from '../config/config';

export default function roleValidator() {
  const throwErr = new Error('ERROR Role Validation !');

  // const addNewRole = async ({ name, description, menus }) => {
  //   const roles = await Roles().findRole({ name });
  //   if (roles) {
  //     throwErr.statusCode = 400;
  //     throwErr.customMessage = ['Nama ROLES sudah didaftarkan !'];
  //     throw throwErr;
  //   }

  //   const schemaRoles = Joi.object({
  //     description: Joi.string().allow(null, '').max(500),
  //     menus: Joi.array().required(),
  //     name: Joi.string().required().max(50)
  //   });

  //   const { error } = schemaRoles.validate(
  //     { name, description, menus },
  //     { abortEarly: false }
  //   );
  //   if (error) {
  //     throwErr.statusCode = 400;
  //     throwErr.customMessage = error.details.map((el) =>
  //       el.message.replace(/"/g, '')
  //     );
  //     throw throwErr;
  //   }

  //   return true;
  // };

  const updateRole = async ({ _id, name, description, menus, status }) => {
    const roles = await Roles().findRole({ name, notId: _id });

    if (roles) {
      throwErr.statusCode = 400;
      throwErr.customMessage = ['Nama ROLES sudah didaftarkan !'];
      throw throwErr;
    }

    const schemaRoles = Joi.object({
      description: Joi.string().allow(null, '').max(500),
      name: Joi.string().required().max(50),
      status: Joi.string().required().max(10),
      _id: Joi.string().required().max(50)
    });

    const { error } = schemaRoles.validate(
      { _id, name, description, status },
      { abortEarly: false }
    );
    if (error) {
      throwErr.statusCode = 400;
      throwErr.customMessage = error.details.map((el) =>
        el.message.replace(/"/g, '')
      );
      throw throwErr;
    }

    // validation menus
    const isValidAction = (value, helpers) => {
      const accessEnv = config.userMethod;
      const accessValid = accessEnv.split(',');
      // const accessInput = value.split(',');
      const accessInput = value;

      if (_.difference(accessInput, accessValid).length === 0) return value;

      return helpers.error('access.base');
    };

    const objectMenus = Joi.object().keys({
      groupMenu: Joi.string().max(50).required(),
      groupSort: Joi.number().required(),
      label: Joi.string().max(50).required(),
      icon: Joi.string().max(50).required(),
      sort: Joi.number().required(),
      url: Joi.string().max(50).required(),
      actions: Joi.array()
        .max(30)
        .required()
        .custom(isValidAction, 'custom access')
        .message({
          'access.base': 'Action invalid'
        })
    });

    const schemaMenus = Joi.array().items(objectMenus);
    const menusValidate = schemaMenus.validate(menus, { abortEarly: false });
    if (menusValidate.error) {
      throwErr.statusCode = 400;
      throwErr.customMessage = menusValidate.error.details.map((el) =>
        el.message.replace(/"/g, '')
      );
      throw throwErr;
    }

    return true;
  };

  const addNewRole = async ({ name, description, menus, status }) => {
    const schemaRoles = Joi.object({
      description: Joi.string().allow(null, '').max(500),
      status: Joi.string().max(15).required(),
      name: Joi.string().max(35).required()
    });

    // validation name
    const roles = await Roles().findRole({ name });
    if (roles) {
      throwErr.statusCode = 400;
      throwErr.customMessage = ['Nama ROLES sudah didaftarkan !'];
      throw throwErr;
    }

    const { error } = schemaRoles.validate(
      { name, description, status },
      { abortEarly: false }
    );
    if (error) {
      throwErr.statusCode = 400;
      throwErr.customMessage = error.details.map((el) =>
        el.message.replace(/"/g, '')
      );
      throw throwErr;
    }

    // validation menus
    const isValidAction = (value, helpers) => {
      const accessEnv = config.userMethod;
      const accessValid = accessEnv.split(',');
      // const accessInput = value.split(',');
      const accessInput = value;

      if (_.difference(accessInput, accessValid).length === 0) return value;

      return helpers.error('access.base');
    };

    const objectMenus = Joi.object().keys({
      groupMenu: Joi.string().max(50).required(),
      groupSort: Joi.number().required(),
      label: Joi.string().max(50).required(),
      icon: Joi.string().max(50).required(),
      sort: Joi.number().required(),
      url: Joi.string().max(50).required(),
      actions: Joi.array()
        .max(30)
        .required()
        .custom(isValidAction, 'custom access')
        .message({
          'access.base': 'Action invalid'
        })
    });

    const schemaMenus = Joi.array().items(objectMenus);
    const menusValidate = schemaMenus.validate(menus, { abortEarly: false });
    if (menusValidate.error) {
      throwErr.statusCode = 400;
      throwErr.customMessage = menusValidate.error.details.map((el) =>
        el.message.replace(/"/g, '')
      );
      throw throwErr;
    }

    return true;
  };

  return {
    updateRole,
    addNewRole
  };
}
