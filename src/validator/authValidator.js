import Joi from 'joi';

// import userModel from '../models/userModel';
import adminModel from '../models/adminModel';
import authService from '../services/authService';

export default function authValidator() {
  const authAdmin = async ({ email, password }) => {
    const throwErr = new Error('Auth ADMIN Validation');

    const schemaRoles = Joi.object({
      email: Joi.string().required().max(45),
      password: Joi.string().required().max(45)
    });

    const { error } = schemaRoles.validate(
      { email, password },
      { abortEarly: false }
    );
    if (error) {
      throwErr.statusCode = 400;
      throwErr.customMessage = error.details.map((el) =>
        el.message.replace(/"/g, '')
      );
      throw throwErr;
    }

    const admin = await adminModel().findAdmin({ email, isActive: true });
    if (!admin) {
      throwErr.statusCode = 400;
      throwErr.customMessage = 'INVALID ADMIN !';
      throw throwErr;
    }

    const isMatch = authService().compare(password, admin.password);
    if (!isMatch) {
      throwErr.statusCode = 400;
      throwErr.customMessage = 'INVALID PASSWORD !';
      throw throwErr;
    }

    return true;
  };

  // const login = async ({ username, password }) => {
  //   const throwErr = new Error('Auth Validation');

  //   const schemaRoles = Joi.object({
  //     username: Joi.string().required().max(30),
  //     password: Joi.string().required().max(30)
  //   });

  //   const { error } = schemaRoles.validate(
  //     { username, password },
  //     { abortEarly: false }
  //   );
  //   if (error) {
  //     throwErr.statusCode = 400;
  //     throwErr.customMessage = error.details.map((el) =>
  //       el.message.replace(/"/g, '')
  //     );
  //     throw throwErr;
  //   }

  //   const user = await userModel().findUser({ username });
  //   if (!user) {
  //     throwErr.statusCode = 400;
  //     throwErr.customMessage = 'INVALID USER !';
  //     throw throwErr;
  //   }

  //   const isMatch = authService().compare(password, user.password);
  //   if (!isMatch) {
  //     throwErr.statusCode = 400;
  //     throwErr.customMessage = 'INVALID PASSWORD !';
  //     throw throwErr;
  //   }

  //   return true;
  // };

  return {
    // login,
    authAdmin
  };
}
