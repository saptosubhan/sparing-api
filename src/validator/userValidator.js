import Joi from 'joi';
import userModel from '../models/userModel';
import roleModel from '../models/roleModel';
import companyModel from '../models/companyModel';

export default function userValidator() {
  const throwErr = new Error('Auth Validation');

  const addNewUser = async ({
    fullname,
    email,
    password,
    status,
    roles,
    companies
  }) => {
    const schemaRoles = Joi.object({
      email: Joi.string().required().email(),
      fullname: Joi.string().required().max(30),
      password: Joi.string().required().max(30),
      status: Joi.string().required().max(10),
      roles: Joi.string().required().max(30),
      companies: Joi.string().required().max(30)
    });

    const { error } = schemaRoles.validate(
      { fullname, email, password, status, roles, companies },
      { abortEarly: false }
    );
    if (error) {
      throwErr.statusCode = 400;
      throwErr.customMessage = error.details.map((el) =>
        el.message.replace(/"/g, '')
      );
      throw throwErr;
    }

    // validate email
    const user = await userModel().findUser({ email });
    if (user) {
      throwErr.statusCode = 400;
      throwErr.customMessage = 'EMAIL sudah terdaftar !';
      throw throwErr;
    }

    // validate roles
    const role = await roleModel().findRole({ _id: roles });
    if (!role) {
      throwErr.statusCode = 400;
      throwErr.customMessage = 'ROLE INVALID !';
      throw throwErr;
    }

    // validate company
    const company = await companyModel().findCompany({ _id: companies });
    if (!company) {
      throwErr.statusCode = 400;
      throwErr.customMessage = 'COMPANY INVALID !';
      throw throwErr;
    }

    return true;
  };

  const updateUser = async ({
    fullname,
    email,
    password,
    status,
    roles,
    companies,
    _id
  }) => {
    const schemaRoles = Joi.object({
      password: Joi.string().allow(null, '').max(50),
      email: Joi.string().required().email(),
      fullname: Joi.string().required().max(30),
      status: Joi.string().required().max(10),
      roles: Joi.string().required().max(30),
      companies: Joi.string().required().max(30),
      _id: Joi.string().required().max(30)
    });

    const { error } = schemaRoles.validate(
      { fullname, email, password, status, roles, companies, _id },
      { abortEarly: false }
    );
    if (error) {
      throwErr.statusCode = 400;
      throwErr.customMessage = error.details.map((el) =>
        el.message.replace(/"/g, '')
      );
      throw throwErr;
    }

    // validate email
    const user = await userModel().findUser({ email, notId: _id });
    if (user) {
      throwErr.statusCode = 400;
      throwErr.customMessage = 'EMAIL sudah terdaftar !';
      throw throwErr;
    }

    // validate roles
    const role = await roleModel().findRole({ _id: roles });
    if (!role) {
      throwErr.statusCode = 400;
      throwErr.customMessage = 'ROLE INVALID !';
      throw throwErr;
    }

    // validate company
    const company = await companyModel().findCompany({ _id: companies });
    if (!company) {
      throwErr.statusCode = 400;
      throwErr.customMessage = 'COMPANY INVALID !';
      throw throwErr;
    }

    return true;
  };

  return {
    addNewUser,
    updateUser
  };
}
