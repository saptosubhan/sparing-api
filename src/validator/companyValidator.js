import Joi from 'joi';

import Schema from '../database/mongoDB/collections';

const { Companies } = Schema;

export default function companyValidator() {
  const throwErr = new Error('ERROR Company Validation !');

  const addNewCompany = async ({
    name,
    address,
    phone,
    status,
    expiredDate
  }) => {
    // tokopedia change address
    const schema = Joi.object({
      name: Joi.string().max(30).required(),
      address: Joi.string().max(200).required(),
      phone: Joi.string().max(14).required(),
      status: Joi.string().max(10).required(),
      expiredDate: Joi.string().max(9).max(10).required()
    });

    const { error } = schema.validate(
      { name, address, phone, status, expiredDate },
      { abortEarly: false }
    );
    if (error) {
      throwErr.statusCode = 400;
      throwErr.customMessage = error.details.map((el) =>
        el.message.replace(/"/g, '')
      );
      throw throwErr;
    }

    // validation duplicate
    const companies = await Companies.findOne({ name });
    if (companies) {
      throwErr.statusCode = 400;
      throwErr.customMessage = ['PERUSAHAAN sudah didaftarkan !'];
      throw throwErr;
    }

    return true;
  };

  const updateCompany = async ({
    _id,
    name,
    address,
    phone,
    status,
    expiredDate
  }) => {
    const schema = Joi.object({
      _id: Joi.string().required().max(40),
      name: Joi.string().max(30).required(),
      address: Joi.string().max(200).required(),
      phone: Joi.string().min(5).max(14).required(),
      status: Joi.string().max(10).required(),
      expiredDate: Joi.string().max(9).max(10).required()
    });

    const { error } = schema.validate(
      { _id, name, address, phone, status, expiredDate },
      { abortEarly: false }
    );
    if (error) {
      throwErr.statusCode = 400;
      throwErr.customMessage = error.details.map((el) =>
        el.message.replace(/"/g, '')
      );
      throw throwErr;
    }

    // validate duplicate name
    const companies = await Companies.findOne({
      name: {
        $regex: name,
        $options: 'i'
      },
      _id: { $ne: _id }
    });
    console.log(_id, '<=== === === _id');
    console.log(companies, '<=== === === companies');
    if (companies) {
      throwErr.statusCode = 400;
      throwErr.customMessage = ['Nama companies sudah didaftarkan !'];
      throw throwErr;
    }

    return true;
  };

  return {
    updateCompany,
    addNewCompany
  };
}
