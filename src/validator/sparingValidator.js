// const Joi = require('joi');
// const _ = require('lodash');
import Joi from 'joi';

export default function sparingValidator() {
  const throwErr = new Error('Sparing Validation');

  const postData = ({ token }) => {
    const schemaRoles = Joi.object({
      token: Joi.string().required()
    });

    const { error } = schemaRoles.validate({ token }, { abortEarly: false });
    if (error) {
      throwErr.statusCode = 400;
      throwErr.customMessage = error.details.map((el) =>
        el.message.replace(/"/g, '')
      );

      throw throwErr;
    }

    return true;
  };

  return {
    postData
  };
}
