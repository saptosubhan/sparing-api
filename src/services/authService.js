import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';

import config from '../config/config';

export default function authService() {
  const encryptPassword = (password) => {
    const salt = bcrypt.genSaltSync(10);
    return bcrypt.hashSync(password, salt);
  };

  const compare = (password, hashedPassword) =>
    bcrypt.compareSync(password, hashedPassword);

  const verifyToken = (token) => jwt.verify(token, config.jwtSecret) || null;

  const generateToken = (payload) =>
    jwt.sign(payload, config.jwtSecret, {
      expiresIn: config.jwtExpToken
    });

  const generateRefreshToken = (payload) =>
    jwt.sign(payload, config.jwtSecret, {
      expiresIn: config.jwtExpToken
    });

  return {
    encryptPassword,
    compare,
    verifyToken,
    generateToken,
    generateRefreshToken
  };
}
