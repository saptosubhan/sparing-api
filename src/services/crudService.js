/**
 *
 * @request {*} req.body, dll
 * @action {*} [create, read, update, delete]
 * @returns
 */
exports.createObject = ({ request, action = 'create' }) => {
  const reqBody = request.body || request;
  const user = request?.auth?.email || 'administrator';
  let result = false;

  if (typeof reqBody === 'object') {
    reqBody.updatedBy = user;

    if (action === 'create') {
      reqBody.createdAt = Date.now();
      reqBody.createdBy = user;
    }

    if (action === 'delete') {
      reqBody.deletedAt = Date.now();
    }

    result = reqBody;
  }

  if (Array.isArray(reqBody)) {
    result = reqBody.map((el) => {
      el.createdAt = Date.now();
      el.createdBy = user;
      el.updatedBy = user;

      if (action === 'delete') {
        el.deletedAt = Date.now();
      }

      return el;
    });
  }

  return result;
};
