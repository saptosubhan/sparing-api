import adminController from '../controllers/adminController';
import Authentication from '../middlewares/Authentication';
import Authorization from '../middlewares/Authorization';

export default function roleRouter(express) {
  const router = express.Router();

  router
    .route('/')
    .post([Authentication, Authorization], adminController().addNewAdmin);
  router
    .route('/')
    .get([Authentication, Authorization], adminController().fetchAdmins);
  router
    .route('/')
    .put([Authentication, Authorization], adminController().updateAdmin);

  return router;
}
