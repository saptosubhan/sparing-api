import sparingController from '../controllers/sparingController';
import Authentication from '../middlewares/Authentication';
import Authorization from '../middlewares/Authorization';

export default function sparingRouter(express) {
  const router = express.Router();

  // router.route('/').get(authMiddleware, sparingController.fetchUsersByProperty);
  // API logger
  router.route('/key').get(sparingController().getKey);
  router.route('/postdata').post(sparingController().postData);
  router.route('/postdata-test').post(sparingController().postDataTest);

  // API frontend
  router
    .route('/logs')
    .get([Authentication, Authorization], sparingController().getLogs);
  router
    .route('/charts')
    .get([Authentication, Authorization], sparingController().getCharts);

  return router;
}
