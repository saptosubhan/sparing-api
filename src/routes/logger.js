import loggerController from '../controllers/loggerController';
import Authentication from '../middlewares/Authentication';
import Authorization from '../middlewares/Authorization';

export default function loggerRouter(express) {
  const router = express.Router();

  router
    .route('/')
    .get([Authentication, Authorization], loggerController().fetchLoggers);
  router
    .route('/')
    .post([Authentication, Authorization], loggerController().addNewLogger);
  router
    .route('/')
    .put([Authentication, Authorization], loggerController().updateLogger);

  return router;
}
