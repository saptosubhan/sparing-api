import menuController from '../controllers/menuController';
import Authentication from '../middlewares/Authentication';
import Authorization from '../middlewares/Authorization';

export default function roleRouter(express) {
  const router = express.Router();

  // GET enpdpoints
  router
    .route('/')
    .get([Authentication, Authorization], menuController().fetchMenus);
  router
    .route('/')
    .post([Authentication, Authorization], menuController().addNewMenu);
  // router.route('/:id').get(menuController().fetchroleById);
  // router.route('/').get([authentication, authorization], menuController.fetchrolesByProperty);

  return router;
}
