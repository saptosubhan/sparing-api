import authController from '../controllers/authController';

export default function roleRouter(express) {
  const router = express.Router();

  router.route('/admin').post(authController().authAdmin);

  router.route('/refreshtoken').post(authController().refreshToken);

  return router;
}
