import userController from '../controllers/userController';
import Authentication from '../middlewares/Authentication';
import Authorization from '../middlewares/Authorization';

export default function userRouter(express) {
  const router = express.Router();

  // GET enpdpoints
  router
    .route('/')
    .get([Authentication, Authorization], userController().fetchUsers);
  router
    .route('/')
    .post([Authentication, Authorization], userController().addNewUser);
  router
    .route('/')
    .put([Authentication, Authorization], userController().updateUser);

  return router;
}
