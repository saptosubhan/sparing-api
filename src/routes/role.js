import roleController from '../controllers/roleController';
import Authentication from '../middlewares/Authentication';
import Authorization from '../middlewares/Authorization';

// const { authentication, authorization } = Middleware;
export default function roleRouter(express) {
  const router = express.Router();

  router
    .route('/')
    .post([Authentication, Authorization], roleController().addNewRole);
  router
    .route('/')
    .get([Authentication, Authorization], roleController().fetchRoles);
  router
    .route('/')
    .put([Authentication, Authorization], roleController().updateRole);

  return router;
}
