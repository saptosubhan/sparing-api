import config from '../config/config';

// import menuRouter from './menu';
import authRouter from './auth';
import roleRouter from './role';
import sparingRouter from './sparing';
import adminRouter from './admin';
import companyRouter from './company';
import userRouter from './user';
import loggerRouter from './logger';

// stelah buat api users buat authentication & authorization
export default function routes(app, express) {
  app.get('/key', (req, res) => {
    res.status(301).redirect(`${config.baseUrl}/sparing/key`);
  });
  app.post('/postdata', (req, res) => {
    res.status(301).redirect(`${config.baseUrl}/sparing/postdata-test`);
  });

  app.use('/auth', authRouter(express));
  app.use('/roles', roleRouter(express));
  app.use('/admins', adminRouter(express));
  app.use('/sparing', sparingRouter(express));
  app.use('/company', companyRouter(express));
  app.use('/users', userRouter(express));
  app.use('/loggers', loggerRouter(express));

  app.use((req, res) => {
    res.status(404);

    return res.json({ error: 'Not found, KOSONG !' });
  });
}
