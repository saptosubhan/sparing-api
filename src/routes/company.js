import companyController from '../controllers/companyController';
import Authentication from '../middlewares/Authentication';
import Authorization from '../middlewares/Authorization';

export default function companyRouter(express) {
  const router = express.Router();

  router
    .route('/')
    .post([Authentication, Authorization], companyController().addNewCompany);
  router
    .route('/')
    .get([Authentication, Authorization], companyController().fetchCompanies);
  router
    .route('/')
    .put([Authentication, Authorization], companyController().updateCompany);

  return router;
}
