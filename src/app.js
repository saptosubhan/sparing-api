import express from 'express';
import mongoose from 'mongoose';
import config from './config/config';
import expressConfig from './config/express'; // masukin ke config juga
import serverConfig from './config/server';

import routes from './routes';
import mongoDbConnection from './database/mongoDB/connection';

// middlewares
import errorHandlingMiddleware from './middlewares/errorHandlingMiddleware';

const app = express();
const server = require('http').createServer(app);

// express.js configuration (middlewares etc.)
expressConfig(app);

// server configuration and start
serverConfig(app, mongoose, server, config).startServer();

// DB configuration and connection create
mongoDbConnection(mongoose, config, {
  // useCreateIndex: true,
  // useNewUrlParser: true,
  // autoReconnect: true,
  // reconnectTries: Number.MAX_VALUE,
  // reconnectInterval: 10000,
  // connectTimeoutMS: 1000,
  autoIndex: false, // Don't build indexes
  maxPoolSize: 10, // Maintain up to 10 socket connections
  serverSelectionTimeoutMS: 5000, // Keep trying to send operations for 5 seconds
  socketTimeoutMS: 45000 // Close sockets after 45 seconds of inactivity
}).connectToMongo();

// routes for each endpoint
routes(app, express);

// error handling middleware
app.use(errorHandlingMiddleware);

// Expose app
export default app;
