import companyValidator from '../validator/companyValidator';
import companyModel from '../models/companyModel';

export default function companyController() {
  const addNewCompany = async (req, res, next) => {
    try {
      await companyValidator().addNewCompany(req.body);
      await companyModel().addNewCompany(req);

      return res.json({ status: 200, message: 'Success Add Company' });
    } catch (error) {
      console.log(error, '<=== === === error addNewCompany');
      return next(error);
    }
  };

  const fetchCompanies = async (req, res, next) => {
    try {
      const { first, rows } = req.query;
      const data = await companyModel().findCompanies({ first, rows });

      return res.json({ status: 200, message: 'Ok', data });
    } catch (error) {
      return next(error);
    }
  };

  const updateCompany = async (req, res, next) => {
    try {
      await companyValidator().updateCompany(req.body);
      await companyModel().updateCompany(req);

      return res.json({ status: 200, message: 'Success update company !' });
    } catch (error) {
      console.log(error, '<=== === === error UPDATE COMPANY');
      return next(error);
    }
  };

  return {
    fetchCompanies,
    addNewCompany,
    updateCompany
  };
}
