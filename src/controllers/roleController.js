import roleValidator from '../validator/roleValidator';
import roleModel from '../models/roleModel';

export default function roleController() {
  const addNewRole = async (req, res, next) => {
    const { name, description, menus, status } = req.body;

    try {
      await roleValidator().addNewRole({ name, description, menus, status });
      await roleModel().addNewRole(req);

      return res.json({ status: 200, message: 'Success Add Role' });
    } catch (error) {
      console.log(error, '<=== === === error addNewRole');
      return next(error);
    }
  };

  const fetchRoles = async (req, res, next) => {
    try {
      const data = await roleModel().findRoles();

      return res.json({ status: 200, message: 'Ok', data });
    } catch (error) {
      return next(error);
    }
  };

  const updateRole = async (req, res, next) => {
    const { _id, name, description, menus, status } = req.body;

    try {
      await roleValidator().updateRole({
        _id,
        name,
        description,
        menus,
        status
      });
      await roleModel().updateRole({ _id, name, description, menus, status });

      return res.json({ status: 200, message: 'Success update role !' });
    } catch (error) {
      console.log(error, '<=== === === error UPDATE ROLE');
      return next(error);
    }
  };

  return {
    addNewRole,
    updateRole,
    fetchRoles
  };
}
