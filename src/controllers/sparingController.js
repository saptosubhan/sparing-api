import winston from 'winston';
import moment from 'moment';

import sparingValidator from '../validator/sparingValidator';
import sparingModel from '../models/sparingModel';
import authService from '../services/authService';
import config from '../config/config';

const { combine, timestamp, json } = winston.format;
const logger = winston.createLogger({
  format: combine(timestamp(), json()),
  transports: [
    new winston.transports.File({
      filename: 'logs/error.log'
    })
  ]
});

export default function sparingController() {
  const getLogs = async (req, res, next) => {
    try {
      const statusMutu = ({ arr, name, value }) => {
        const sensor = arr.filter((val) => val.sensor === name)[0];
        return value <= sensor.min || value >= sensor.max ? 'danger' : 'normal';
      };

      const data = await sparingModel().findLogs(req);
      data.sparing = data.sparing.map((el) => ({
        datetime: moment.unix(el.datetime).format('MMM/DD HH:mm'),
        sensors: [
          {
            name: 'ph',
            value: el.ph,
            status: statusMutu({
              arr: el.loggers.bakuMutu,
              name: 'ph',
              value: el.ph
            })
          },
          {
            name: 'cod',
            value: el.cod,
            status: statusMutu({
              arr: el.loggers.bakuMutu,
              name: 'cod',
              value: el.cod
            })
          },
          {
            name: 'tss',
            value: el.tss,
            status: statusMutu({
              arr: el.loggers.bakuMutu,
              name: 'tss',
              value: el.tss
            })
          },
          {
            name: 'nh3n',
            value: el.nh3n,
            status: statusMutu({
              arr: el.loggers.bakuMutu,
              name: 'nh3n',
              value: el.nh3n
            })
          },
          {
            name: 'debit',
            value: el.debit,
            status: statusMutu({
              arr: el.loggers.bakuMutu,
              name: 'debit',
              value: el.debit
            })
          }
        ]
      }));

      return res.json({ status: 200, message: 'Ok', data });
    } catch (error) {
      return next(error);
    }
  };

  const getCharts = async (req, res, next) => {
    try {
      const { sparing } = await sparingModel().findLogs(req);
      const bakuMutu = sparing.map((el) => el.loggers.bakuMutu)[0];
      const series = sparing.map((el) => ({
        x: moment.unix(el.datetime).format('MMM/DD HH:mm'),
        y: [
          {
            name: 'ph',
            value: el.ph
          },
          {
            name: 'cod',
            value: el.cod
          },
          {
            name: 'tss',
            value: el.tss
          },
          {
            name: 'nh3n',
            value: el.nh3n
          },
          {
            name: 'debit',
            value: el.debit
          }
        ]
      }));

      return res.json({
        status: 200,
        message: 'Ok',
        data: { series, bakuMutu }
      });
    } catch (error) {
      return next(error);
    }
  };

  const getKey = async (req, res, next) => {
    try {
      return res.send(config.keySecret);
    } catch (error) {
      console.log(error, '<=== === === error getKey');
      const errorGetKey = {
        getKey: JSON.stringify(error)
      };
      logger.info(JSON.stringify(errorGetKey));
      return next(error);
    }
  };

  const postDataTest = async (req, res, next) => {
    try {
      const token = req.body?.token;
      await sparingValidator().postData({ token });

      const decoded = authService().verifyToken(token);
      req.body.data = decoded;
      req.body.isDataTest = 1;
      req.body.uidSCI = decoded.data.uid;
      req.body.uidKLHK = '';

      await sparingModel().postData(req);

      return res.json({ status: 200, message: 'Ok' });
    } catch (error) {
      console.log(error, '<=== === === error PostData-TEST');
      const errorPostDataTest = {
        psotDataTest: JSON.stringify(error)
      };
      logger.info(JSON.stringify(errorPostDataTest));
      return next(error);
    }
  };

  const postData = async (req, res, next) => {
    try {
      const token = req.body?.token;
      await sparingValidator().postData({ token });

      const decoded = authService().verifyToken(token);
      req.body.data = decoded;
      req.body.isDataTest = 0;
      req.body.uidSCI = '';
      req.body.uidKLHK = decoded.data.uid;

      await sparingModel().postData(req);

      return res.json({ status: 200, message: 'Ok' });
    } catch (error) {
      console.log(error, '<=== === === error PostData');
      const errorPostData = {
        psotData: JSON.stringify(error)
      };
      logger.info(JSON.stringify(errorPostData));
      return next(error);
    }
  };

  return {
    getKey,
    postData,
    postDataTest,
    getLogs,
    getCharts
  };
}
