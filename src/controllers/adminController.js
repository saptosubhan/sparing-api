import adminValidator from '../validator/adminValidator';
import authService from '../services/authService';
import adminModel from '../models/adminModel';

export default function adminController() {
  const addNewAdmin = async (req, res, next) => {
    const { fullname, email, password, roles } = req.body;

    try {
      await adminValidator().addNewAdmin({ fullname, email, password, roles });
      await adminModel().addNewAdmin({
        fullname,
        email,
        password: authService().encryptPassword(password),
        roles
      });

      return res.json({ status: 200, message: 'Success Add Admin' });
    } catch (error) {
      console.log(error, '<=== === === error addNewAdmin');
      return next(error);
    }
  };

  const fetchAdmins = async (req, res, next) => {
    try {
      const data = await adminModel().findAdmins();

      return res.json({ status: 200, message: 'Ok', data });
    } catch (error) {
      return next(error);
    }
  };

  const updateAdmin = async (req, res, next) => {
    const { _id, fullname, email, password, roles } = req.body;

    try {
      await adminValidator().updateAdmin({
        _id,
        fullname,
        email,
        password,
        roles
      });

      const paramsUpdate = { _id, fullname, email, roles };
      paramsUpdate.password = password
        ? authService().encryptPassword(password)
        : password;

      await adminModel().updateAdmin(paramsUpdate);

      return res.json({ status: 200, message: 'Success update admin !' });
    } catch (error) {
      console.log(error, '<=== === === error UPDATE ADMIN');
      return next(error);
    }
  };

  return {
    addNewAdmin,
    updateAdmin,
    fetchAdmins
  };
}
