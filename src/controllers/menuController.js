// import mongoose from 'mongoose';
import crudService from '../services/crudService';
import Menu from '../database/mongoDB/collections/menu';

export default function userController() {
  const fetchMenus = async (req, res, next) => {
    try {
      const data = await Menu.find();

      return res.json({ status: 200, message: 'Ok', data });
    } catch (error) {
      return next(error);
    }
  };

  // Transaction masih error
  const addNewMenu = async (req, res, next) => {
    try {
      const params = crudService.createObject({ request: req });
      await Menu.deleteMany({});
      await Menu.insertMany(params);

      return res.json({ status: 200, message: 'Ok' });
    } catch (error) {
      return next(error);
    }
  };
  // // Transaction masih error
  // const addNewMenu = async (req, res, next) => {
  //   const sessTrans = await mongoose.startSession();
  //   try {
  //     sessTrans.startTransaction();

  //     await Menu.deleteMany({}, { session: sessTrans });
  //     await Menu.insertMany(req.body, { session: sessTrans });

  //     await sessTrans.commitTransaction();
  //     sessTrans.endSession();

  //     return res.json({ status: 200, message: 'Ok' });
  //   } catch (error) {
  //     await sessTrans.abortTransaction();
  //     sessTrans.endSession();

  //     return next(error);
  //   }
  // };

  return {
    fetchMenus,
    addNewMenu
  };
}
