import loggerValidator from '../validator/loggerValidator';
import CrudService from '../services/crudService';
import loggerModel from '../models/loggerModel';

export default function loggerController() {
  const addNewLogger = async (req, res, next) => {
    try {
      await loggerValidator().addNewLogger(req.body);

      const params = CrudService.createObject({ request: req });
      console.log(params, '<=== === === params');
      await loggerModel().addNewLogger(params);

      return res.json({ status: 200, message: 'Success add logger' });
    } catch (error) {
      console.log(error, '<=== === === error addNewLogger');
      return next(error);
    }
  };

  const updateLogger = async (req, res, next) => {
    try {
      await loggerValidator().updateLogger(req.body);

      const paramsUpdate = CrudService.createObject({
        request: req.body,
        action: 'update'
      });
      console.log(paramsUpdate, '<=== === === paramsUpdate');
      await loggerModel().updateLogger(paramsUpdate);

      return res.json({ status: 200, message: 'Success update logger' });
    } catch (error) {
      console.log(error, '<=== === === error UPDATE');
      return next(error);
    }
  };

  const fetchLoggers = async (req, res, next) => {
    try {
      const data = await loggerModel().findLoggers(req.query);

      return res.json({ status: 200, message: 'success', data });
    } catch (error) {
      return next(error);
    }
  };

  return {
    addNewLogger,
    updateLogger,
    fetchLoggers
  };
}
