import userValidator from '../validator/userValidator';
import authService from '../services/authService';
import CrudService from '../services/crudService';
import userModel from '../models/userModel';

export default function userController() {
  const addNewUser = async (req, res, next) => {
    try {
      console.log(req.body, '<=== === === req.body');
      await userValidator().addNewUser(req.body);

      req.body.password = authService().encryptPassword(req.body.password);
      const params = CrudService.createObject({ request: req });
      await userModel().addNewUser(params);

      return res.json({ status: 200, message: 'Success ad user' });
    } catch (error) {
      console.log(error, '<=== === === error addNewUser');
      return next(error);
    }
  };

  const updateUser = async (req, res, next) => {
    try {
      await userValidator().updateUser(req.body);

      const paramsUpdate = CrudService.createObject({
        request: req.body,
        action: 'update'
      });
      if (paramsUpdate.password) {
        paramsUpdate.password = authService().encryptPassword(
          paramsUpdate.password
        );
      } else {
        delete paramsUpdate.password;
      }

      await userModel().updateUser(paramsUpdate);

      return res.json({ status: 200, message: 'Success update user' });
    } catch (error) {
      console.log(error, '<=== === === error UPDATE');
      return next(error);
    }
  };

  // const fetchUser = async (req, res, next) => {
  //   try {
  //     const data = await userModel().findAll();

  //     return res.json({ status: 200, message: 'Ok', data });
  //   } catch (error) {
  //     return next(error);
  //   }
  // };

  const fetchUsers = async (req, res, next) => {
    try {
      const { first, rows } = req.query;
      const data = await userModel().findUsers({ first, rows });

      return res.json({ status: 200, message: 'success', data });
    } catch (error) {
      return next(error);
    }
  };

  return {
    addNewUser,
    updateUser,
    fetchUsers
    // fetchUser,
  };
}
