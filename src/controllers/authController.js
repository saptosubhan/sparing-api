import authValidator from '../validator/authValidator';
import authService from '../services/authService';
// import userModel from '../models/userModel';
import adminModel from '../models/adminModel';
import roleModel from '../models/roleModel';

export default function userController() {
  const authAdmin = async (req, res, next) => {
    const { email, password } = req.body;

    try {
      await authValidator().authAdmin({ email, password });

      const { fullname, roles, level } = await adminModel().findAdmin({
        email
      });

      const role = await roleModel().findRole({ _id: roles });
      const groupMenu = role.menus
        .sort((a, b) => a.groupSort - b.groupSort) // sort by groupSort
        .map((el) => el.groupMenu); // return just groupMenu
      const menus = [...new Set(groupMenu)].map((el) => ({
        label: el,
        items: role.menus
          .filter((elItemFilter) => elItemFilter.groupMenu === el)
          .sort((a, b) => a.sort - b.sort)
          .map((elItem) => {
            const { label, icon, url, actions } = elItem;
            return { label, icon, to: url, actions };
          })
      }));
      // userlevel = [admin,customer]
      const payload = { email, fullname, level };
      const token = authService().generateToken(payload);

      payload.isRefreshToken = true;
      const refreshToken = authService().generateRefreshToken(payload);
      const menuString = JSON.stringify(menus);

      const data = {
        email,
        fullname,
        menus: btoa(menuString),
        token,
        refreshToken
      };

      return res.json({ status: 200, message: 'Ok', data });
    } catch (error) {
      console.log(error, '<=== === === error');
      return next(error);
    }
  };

  // const login = async (req, res, next) => {
  //   const { username, password } = req.body;

  //   try {
  //     await authValidator().login({ username, password });

  //     const { fullname, roleName } = await userModel().findUser({ username });
  //     const role = await roleModel().findRole({ name: roleName });
  //     const groupMenu = role.menus
  //       .sort((a, b) => a.groupSort - b.groupSort) // sort by groupSort
  //       .map((el) => el.groupMenu); // return just groupMenu
  //     const menus = [...new Set(groupMenu)].map((el) => ({
  //       label: el,
  //       items: role.menus
  //         .filter((elItemFilter) => elItemFilter.groupMenu === el)
  //         .sort((a, b) => a.sort - b.sort)
  //         .map((elItem) => {
  //           const { label, icon, url, actions } = elItem;
  //           return { label, icon, to: url, actions };
  //         })
  //     }));

  //     const payload = { username, fullname, roleName };
  //     const token = authService().generateToken(payload);

  //     payload.isRefreshToken = true;
  //     const refreshToken = authService().generateRefreshToken(payload);

  //     const data = {
  //       username,
  //       fullname,
  //       roleName,
  //       menus,
  //       token,
  //       refreshToken
  //     };

  //     return res.json({ status: 200, message: 'Ok', data });
  //   } catch (error) {
  //     console.log(error, '<=== === === error');
  //     return next(error);
  //   }
  // };

  const refreshToken = async (req, res, next) => {
    try {
      const reqToken = req.body?.requestToken;

      if (!reqToken) {
        throw new Error({
          statusCode: 403,
          customMessage: 'Refresh Token invalid !'
        });
      }

      const token = await authService().verifyToken(reqToken);
      if (!token.isRefreshToken) {
        throw new Error({
          statusCode: 403,
          customMessage: 'Refresh Token invalid !!!'
        });
      }

      const payload = {
        username: token.username,
        fullname: token.fullname,
        roleName: token.roleName
      };

      const newAccessToken = authService().generateToken(payload);
      const newRefreshToken = authService().generateRefreshToken(payload);

      return res.status(200).json({
        accessToken: newAccessToken,
        refreshToken: newRefreshToken
      });
    } catch (error) {
      console.log(error, '<=== === === error');
      return next(error);
    }
  };

  return {
    refreshToken,
    authAdmin
    // login
  };
}
