require('dotenv').config();

export default {
  baseUrl: process.env.BASE_URL,
  port: process.env.PORT,
  ip: process.env.HOST,
  mongo: {
    uri: process.env.MONGO_URL,
    debug: process.env.MONGO_DEBUG
  },
  userMethod: process.env.USER_METHOD,
  jwtSecret: process.env.JWT_SECRET,
  jwtExpToken: process.env.JWT_EXP_TOKEN,
  jwtExpRefreshToken: process.env.JWT_EXP_REFRESH_TOKEN,
  keySecret: process.env.KEY_SECRET
  // port: 3030,
  // ip: '0.0.0.0',
  // mongo: {
  //   uri: 'mongodb+srv://root:root@cluster0.5rx1xut.mongodb.net/project-testing?retryWrites=true&w=majority'
  // },
  // jwtSecret: 'Sci@2020!±@£^%)@90p0123ooi',
  // jwtExpToken: '360d',
  // jwtExpRefreshToken: '180d',
  // keySecret: 'Sci@2020!±@£^%)@90p0123ooi'
};
