import express from 'express';
import morgan from 'morgan';
import compression from 'compression';
import helmet from 'helmet';
import cors from 'cors';

export default function expressConfig(app) {
  // security middleware
  app.use(helmet());
  app.use(compression());
  app.use(express.json({ limit: '7mb' }));
  app.use(
    cors({
      credentials: true,
      origin: true
    })
  );
  app.use(
    express.urlencoded({
      limit: '7mb',
      extended: true,
      parameterLimit: 50000
    })
  );

  app.disable('x-powered-by');
  app.use((req, res, next) => {
    // Website you wish to allow to connect
    // res.setHeader('Access-Control-Allow-Origin', 'http://some-accepted-origin');
    // Request methods you wish to allow
    res.setHeader(
      'Access-Control-Allow-Methods',
      'GET, POST, OPTIONS, PUT, PATCH, DELETE'
    );
    // Request headers you wish to allow
    res.setHeader(
      'Access-Control-Allow-Headers',
      'X-Requested-With, Content-type, Authorization, Cache-control, Pragma'
    );
    // Pass to next layer of middleware
    next();
  });

  app.use(morgan('combined'));
}
