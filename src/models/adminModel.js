import CrudService from '../services/crudService';
import Schema from '../database/mongoDB/collections';

const { Admins } = Schema;

export default function adminModels() {
  const findAdmins = async () => {
    const admins = await Admins.find()
      .sort({
        datetime: -1
      })
      .populate({
        path: 'roles'
      });

    return admins;
  };

  const findAdmin = async (params) => {
    const where = {};
    if (params.email) {
      where.email = {
        $regex: params.email,
        $options: 'i'
      };
    }

    if (params.notId) {
      where._id = {
        $ne: params.notId
      };
    }
    if (params.isActive) {
      where.deletedAt = null;
    }

    const result = await Admins.findOne(where);

    return result;
  };

  const addNewAdmin = (request) => {
    const newAdmin = new Admins(CrudService.createObject({ request }));

    return newAdmin.save();
  };

  const updateAdmin = async ({ _id, fullname, password, email, roles }) => {
    const paramUpdate = { _id, fullname, password, email, roles };
    if (!password) {
      delete paramUpdate.password;
    }

    await Admins.update(
      { _id },
      CrudService.createObject({ request: paramUpdate, action: 'update' })
    );

    return true;
  };

  return {
    findAdmins,
    findAdmin,
    addNewAdmin,
    updateAdmin
  };
}
