import Schema from '../database/mongoDB/collections';
import CrudService from '../services/crudService';

const { SparingLogs } = Schema;
/**
 *
 * bedain data tes dan real cukup dari token saja, gunakan token key yg berbeda,
 * ada konfig di front end untuk merubah token
 */
export default function sparingModels() {
  const findLog = async () => {
    const where = {};
    const sparing = await SparingLogs.findOne({ where })
      .sort({
        datetime: -1
      })
      .populate({
        path: 'loggers',
        select: 'bakuMutu'
      });

    return sparing;
  };

  const findLogs = async (req) => {
    const { first, rows } = req.query;

    const where = {};
    // if (params.name) {
    //   where.name = params.name;
    // }

    const totalRecords = await SparingLogs.find({ where }).count();
    const sparing = await SparingLogs.find({ where })
      .populate({
        path: 'loggers',
        select: 'bakuMutu'
      })
      .skip(first)
      .limit(rows)
      .sort({ datetime: -1 });

    return { totalRecords, sparing };
  };

  const postData = (request) => {
    const newData = CrudService.createObject({ request });

    newData.uidSCI = request.body.uidSCI;
    newData.uidKLHK = request.body.uidKLHK;
    newData.dataTest = request.body.dataTest;
    newData.datetimeLogger = newData.data.datetime;
    newData.ph = newData.data.pH;
    newData.cod = newData.data.cod;
    newData.tss = newData.data.tss;
    newData.nh3n = newData.data.nh3n;
    newData.debit = newData.data.debit;

    delete newData.data;

    const sparing = new SparingLogs(newData);

    return sparing.save();
  };

  return {
    postData,
    findLogs,
    findLog
  };
}
