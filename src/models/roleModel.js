import CrudService from '../services/crudService';
import Schema from '../database/mongoDB/collections';

const { Roles } = Schema;

export default function roleModels() {
  const findRoles = async () => {
    // return await Role.find();
    const roles = await Roles.find().sort({
      datetime: -1
    });

    return roles;
  };

  const findRole = async (params) => {
    const where = {};
    if (params.name) {
      where.name = {
        $regex: params.name,
        $options: 'i'
      };
    }

    if (params.notId) {
      where._id = {
        $ne: params.notId
      };
    }

    if (params._id) {
      where._id = params._id;
    }

    const result = await Roles.findOne(where);

    return result;
  };

  const addNewRole = (request) => {
    const newRole = new Roles(CrudService.createObject({ request }));

    return newRole.save();
  };

  const updateRole = async ({ _id, name, description, menus, status }) => {
    const paramUpdate = CrudService.createObject({
      request: { name, description, menus, status },
      action: 'update'
    });

    await Roles.replaceOne({ _id }, paramUpdate);

    return true;
  };
  // // //=== START TRANSACTION
  // const updateRole = async (request) => {
  //   const sessTrans = await mongoose.startSession();
  //   try {
  //     sessTrans.startTransaction();

  //     const data = CrudService.createObject({ request });

  //     await Roles.deleteOne(
  //       { name: request.body.name },
  //       { session: sessTrans }
  //     );
  //     await Roles.insertMany([data], {
  //       session: sessTrans
  //     });

  //     const trx = await sessTrans.commitTransaction();
  //     // eslint-disable-next-line eqeqeq
  //     if (trx?.ok != 1) {
  //       await sessTrans.abortTransaction();

  //       throw new Error({
  //         statusCode: 500,
  //         customMessage: 'Terjadi kegagalan, silahkan coba lagi !'
  //       });
  //     }
  //     sessTrans.endSession();

  //     return true;
  //   } catch (error) {
  //     await sessTrans.abortTransaction();
  //     sessTrans.endSession();

  //     throw new Error(error);
  //   }
  // };
  // // //=== END TRANSACTION

  return {
    findRoles,
    findRole,
    addNewRole,
    updateRole
  };
}
