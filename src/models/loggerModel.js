import Schema from '../database/mongoDB/collections';

const { Loggers } = Schema;

export default function loggerModels() {
  const addNewLogger = (params) => {
    const newLogger = new Loggers(params);

    return newLogger.save();
  };

  const findLoggers = async (query) => {
    const where = {};
    const first = query?.first ?? 0;
    const rows = query?.rows ?? 10;

    if (query?.companies) {
      where.companies = query?.companies;
    }

    const totalRecords = await Loggers.find({ where }).count();
    const loggers = await Loggers.find(
      { where },
      'uid uidAlias address coordinates sensors status'
    )
      .skip(first)
      .limit(rows);

    return { totalRecords, loggers };
  };

  const updateLogger = async (request) => {
    await Loggers.update({ _id: request._id }, request);

    return true;
  };

  const findLogger = async (params) => {
    const where = {};
    if (params.email) {
      where.email = {
        $regex: params.email,
        $options: 'i'
      };
    }

    if (params.notId) {
      where._id = {
        $ne: params.notId
      };
    }

    const result = await Loggers.findOne(where);

    return result;
  };

  return {
    findLoggers,
    findLogger,
    addNewLogger,
    updateLogger
  };
}
