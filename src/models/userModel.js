import Schema from '../database/mongoDB/collections';

const { Users } = Schema;

export default function userModels() {
  const addNewUser = (params) => {
    const newUser = new Users(params);

    return newUser.save();
  };

  const findUsers = async ({ first = 0, rows = 1 }) => {
    const where = {};

    const totalRecords = await Users.find({ where }).count();
    const users = await Users.find(
      { where },
      'fullname email roles status level'
    )
      .populate({
        path: 'roles',
        select: 'name description status menus'
      })
      .populate({
        path: 'companies',
        select: 'name address phone status expiredDate loggers'
      })
      .skip(first || 0)
      .limit(rows || 1)
      .sort({ name: -1 });

    return { totalRecords, users };
  };

  const updateUser = async (request) => {
    await Users.update({ _id: request._id }, request);

    return true;
  };

  const findUser = async (params) => {
    const where = {};
    if (params.email) {
      where.email = {
        $regex: params.email,
        $options: 'i'
      };
    }

    if (params.notId) {
      where._id = {
        $ne: params.notId
      };
    }

    if (params.isActive) {
      where.deletedAt = null;
    }

    const result = await Users.findOne(where);

    return result;
  };

  return {
    findUsers,
    findUser,
    addNewUser,
    updateUser
  };
}
