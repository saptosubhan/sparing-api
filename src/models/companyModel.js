import Schema from '../database/mongoDB/collections';
import CrudService from '../services/crudService';

const { Companies } = Schema;

export default function companyModel() {
  const addNewCompany = (request) => {
    const newRole = new Companies(CrudService.createObject({ request }));

    return newRole.save();
  };

  const findCompanies = async ({ first, rows }) => {
    const where = {};
    // if (params.name) {
    //   where.name = params.name;
    // }

    const totalRecords = await Companies.find({ where }).count();
    const sparing = await Companies.find({ where })
      .populate({
        path: 'loggers'
        // select: 'bakuMutu'
      })
      .skip(first || 0)
      .limit(rows || 1)
      .sort({ name: -1 });

    return { totalRecords, sparing };
  };

  const updateCompany = async (request) => {
    const paramUpdate = CrudService.createObject({
      request,
      action: 'update'
    });

    await Companies.update({ _id: request._id }, paramUpdate);

    return true;
  };

  const findCompany = async (params) => {
    const where = {};
    if (params.name) {
      where.name = {
        $regex: params.name,
        $options: 'i'
      };
    }

    if (params.notId) {
      where._id = {
        $ne: params.notId
      };
    }

    if (params._id) {
      where._id = params._id;
    }

    if (params.isActive) {
      where.deletedAt = null;
    }

    const result = await Companies.findOne(where);

    return result;
  };

  return {
    addNewCompany,
    updateCompany,
    findCompanies,
    findCompany
  };
}
