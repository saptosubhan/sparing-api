import Schema from '../database/mongoDB/collections';

const { Admins, Users } = Schema;
const Authorization = async (req, res, next) => {
  try {
    const accessModule = req.header('Access-module');
    const accessAction = req.header('Access-action');
    if (!accessModule || !accessAction) {
      throw new Error('No access found');
    }

    const { email, level } = req.auth;
    const model = level === 'admin' ? Admins : Users;
    const userValid = await model
      .findOne({
        email,
        deletedAt: null
      })
      .populate({
        path: 'roles',
        match: {
          'menus.label': accessModule,
          'menus.actions': accessAction
        }
      });

    if (userValid.status !== 'active') {
      throw new Error('USER not active !');
    }

    if (!userValid.roles) {
      throw new Error('INVALID module access');
    }

    next();
  } catch (err) {
    console.log(err, '<=== === === err AUTHORIZATION');
    next(err);
  }
};

export default Authorization;
