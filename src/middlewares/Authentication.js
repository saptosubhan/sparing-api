import authService from '../services/authService';

export default function Authentication(req, res, next) {
  const token = req.header('Authorization');
  if (!token) {
    throw new Error('No access token found');
  }
  if (token.split(' ')[0] !== 'Bearer') {
    throw new Error('Invalid access token');
  }

  const decoded = authService().verifyToken(token.split(' ')[1]);
  if (!decoded) {
    throw new Error('Token not valid');
  }

  req.auth = {
    email: decoded.email,
    fullname: decoded.fullname,
    level: decoded.level
  };

  next();
}
