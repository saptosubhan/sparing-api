// eslint-disable-next-line no-unused-vars
export default function errorHandlingMiddlware(err, req, res, next) {
  const statusCode = err.statusCode || 500;
  const errMessage =
    err.customMessage || err.reason?.toString() || 'Terjadi Kegagalan server !';
  console.log(err, '<=== === === err');

  return res.status(statusCode).json({
    status: statusCode,
    errors: Array.isArray(errMessage) ? errMessage : [errMessage]
  });
}
