import mongoose from 'mongoose';

const { Schema } = mongoose;

const LoggersSchema = new Schema(
  {
    uid: {
      type: String
    },
    uidAlias: {
      type: String
    },
    address: {
      type: String
    },
    // [latitude, longitude]
    coordinates: {
      type: Array
    },
    companies: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Companies'
    },
    sensors: {
      type: Array,
      required: true
    },
    status: {
      type: String,
      required: true
    },
    createdAt: {
      type: Date,
      required: true
    },
    createdBy: {
      type: String,
      required: true
    },
    updatedAt: {
      type: Date,
      default: Date.now
    },
    updatedBy: {
      type: String,
      required: true
    },
    deletedAt: {
      type: Date,
      default: null
    }
  },
  {
    versionKey: false
  }
);

const Loggers = mongoose.model('Loggers', LoggersSchema);

Loggers.ensureIndexes((err) => err || true);

export default Loggers;
