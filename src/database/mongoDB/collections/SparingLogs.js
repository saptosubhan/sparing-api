import mongoose from 'mongoose';

const { Schema } = mongoose;

const SparingLogsSchema = new Schema(
  {
    loggers: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Loggers'
    },
    companies: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Companies'
    },
    dataTest: {
      type: Number,
      default: 0 // 0 real, 1 test
    },
    token: {
      type: String
    },
    uid: {
      type: String
    },
    datetime: {
      type: Number
    },
    ph: {
      type: Number
    },
    cod: {
      type: Number
    },
    tss: {
      type: Number
    },
    nh3n: {
      type: Number
    },
    debit: {
      type: Number
    },
    createdAt: {
      type: Date,
      required: true
    },
    createdBy: {
      type: String,
      required: true
    },
    updatedAt: {
      type: Date,
      default: Date.now
    },
    updatedBy: {
      type: String,
      required: true
    },
    deletedAt: {
      type: Date,
      default: null
    }
  },
  {
    versionKey: false
  }
);

const SparingLogs = mongoose.model('SparingLogs', SparingLogsSchema);

SparingLogs.ensureIndexes((err) => err || true);

export default SparingLogs;
