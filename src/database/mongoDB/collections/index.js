import SparingLogs from './SparingLogs';
import Companies from './Companies';
import Loggers from './Loggers';
import Admins from './Admins';
import Roles from './Roles';
import Menus from './Menus';
import Users from './Users';

export default {
  SparingLogs,
  Companies,
  Loggers,
  Admins,
  Roles,
  Menus,
  Users
};
