import mongoose from 'mongoose';

const { Schema } = mongoose;
const UsersSchema = new Schema(
  {
    fullname: {
      type: String
    },
    email: {
      type: String,
      required: true,
      // lowercase: true,
      unique: true
    },
    password: {
      type: String,
      required: true
    },
    roles: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Roles'
    },
    companies: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Companies'
    },
    status: {
      type: String,
      default: null
    },
    level: {
      type: String,
      default: 'customer'
    },
    createdAt: {
      type: Date,
      required: true
    },
    createdBy: {
      type: String,
      required: true
    },
    updatedAt: {
      type: Date,
      default: Date.now
    },
    updatedBy: {
      type: String,
      required: true
    },
    deletedAt: {
      type: Date,
      default: null
    }
  },
  {
    versionKey: false
  }
);

// UserSchema.index({ role: 1 });

const Users = mongoose.model('Users', UsersSchema);

Users.ensureIndexes((err) => err || true);

export default Users;
