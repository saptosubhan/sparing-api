import mongoose from 'mongoose';

// eslint-disable-next-line prefer-destructuring
const Schema = mongoose.Schema;
const MenuSchema = new Schema(
  {
    groupMenu: {
      type: String,
      required: true
    },
    label: {
      type: String,
      unique: true,
      required: true
    },
    name: {
      type: String,
      unique: true,
      default() {
        return this.label.toLowerCase().split(' ').join('_');
      }
    },
    icon: {
      type: String,
      required: true
    },
    sort: {
      type: Number,
      required: true
    },
    url: {
      type: String,
      required: true
    },
    actions: {
      type: Array,
      required: true
    },
    description: {
      type: String,
      default: null
    },
    createdAt: {
      type: Date,
      default: Date.now
    },
    createdBy: {
      type: String,
      default: 'administrator'
      // required: true
    },
    updatedAt: {
      type: Date,
      default: Date.now
    },
    updatedBy: {
      type: String,
      default: 'administrator'
      // required: true
    },
    deletedAt: {
      type: Date,
      default: null
    }
  },
  {
    versionKey: false
  }
);

// // MenuSchema.index({ name: 1 });

const Menus = mongoose.model('Menus', MenuSchema);

Menus.ensureIndexes((err) => err || true);

export default Menus;
