import mongoose from 'mongoose';

// eslint-disable-next-line prefer-destructuring
const Schema = mongoose.Schema;
const RoleSchema = new Schema(
  {
    name: {
      type: String
    },
    description: {
      type: String
    },
    status: {
      type: String
    },
    menus: {
      type: Array
    },
    createdAt: {
      type: Date,
      required: true
    },
    createdBy: {
      type: String,
      required: true
    },
    updatedAt: {
      type: Date,
      default: Date.now
    },
    updatedBy: {
      type: String,
      required: true
    },
    deletedAt: {
      type: Date,
      default: null
    }
  },
  {
    versionKey: false
  }
);

// export default RoleSchema;
// // RoleSchema.index({ name: 1 });

const Roles = mongoose.model('Roles', RoleSchema);

Roles.ensureIndexes((err) => err || true);

export default Roles;
