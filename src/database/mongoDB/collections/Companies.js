import mongoose from 'mongoose';

const { Schema } = mongoose;

const CompaniesSchema = new Schema(
  {
    name: {
      type: String,
      unique: true
    },
    address: {
      type: String
    },
    phone: {
      type: String
    },
    status: {
      type: String,
      default: 'active'
    },
    expiredDate: {
      type: Date,
      default() {
        const dateNow = new Date();
        return dateNow.setFullYear(dateNow.getFullYear() + 1);
      }
    },
    loggers: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Loggers'
      }
    ],
    createdAt: {
      type: Date,
      required: true
    },
    createdBy: {
      type: String,
      required: true
    },
    updatedAt: {
      type: Date,
      default: Date.now
    },
    updatedBy: {
      type: String,
      required: true
    },
    deletedAt: {
      type: Date,
      default: null
    }
  },
  {
    versionKey: false
  }
);

const CompaniesCollection = mongoose.model('Companies', CompaniesSchema);

CompaniesCollection.ensureIndexes((err) => err || true);

export default CompaniesCollection;
