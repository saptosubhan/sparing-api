import mongoose from 'mongoose';

const { Schema } = mongoose;
const AdminsSchema = new Schema(
  {
    fullname: {
      type: String
    },
    email: {
      type: String,
      // required: true,
      unique: true,
      lowercase: true
    },
    password: {
      type: String,
      required: true
    },
    roles: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Roles'
    },
    level: {
      type: String,
      default: 'admin'
    },
    status: {
      type: String,
      default: 'active'
    },
    createdAt: {
      type: Date,
      required: true
    },
    createdBy: {
      type: String,
      required: true
    },
    updatedAt: {
      type: Date,
      default: Date.now
    },
    updatedBy: {
      type: String,
      required: true
    },
    deletedAt: {
      type: Date,
      default: null
    }
  },
  {
    versionKey: false
  }
);

// AdminsSchema.index({ role: 1 });

const Admins = mongoose.model('Admins', AdminsSchema);

Admins.ensureIndexes((err) => err || true);

export default Admins;
